# POC de consumo da API Rest do Projeto Tokio Marine Aluguel 

## Requisitos

[Java JDK](https://www.oracle.com/technetwork/pt/java/javase/downloads/index.html)

## Como executar o aplicação localmente (Spring Boot)

Execute o comando a seguir na raiz do projeto:

Windows:
```bash
mvnw.cmd spring-boot:run
```
Linux:
```bash
mvnw spring-boot:run
```


O maven fará o build do angular e subirá um servidor na porta 8080.
Assim que o build terminar, a aplicação ficará disponível no endereço:

http://localhost:8080/massificados/fiancaapi

## Login

O senha de acessa à API é fornecido aos Corretores e Parceiros pela equipe de TI da Tokio Marine Seguradora.
Caso ainda não tenha a senha, entre em contato com nossa equipe.