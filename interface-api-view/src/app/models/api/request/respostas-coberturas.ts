import { RespostaCobertura } from './resposta-cobertura';

export class RespostasCoberturas {
  constructor(public coberturas: Array<RespostaCobertura>) { }
}
