export class RespostaCobertura {
  constructor(
    public codigo?: number,
    public valorVerba?: number,
    public valorCaracteristica?: number
  ) {}
}
