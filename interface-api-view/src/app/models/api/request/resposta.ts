import { Campo } from 'src/app/models/api/response/formulario/campo';

export class Resposta {
  public cdCampo: number;
  public cdOpcao: string;
  public dsRespt: string;

  constructor(campo: Campo) {
    this.cdCampo = campo.codigo;
    this.dsRespt = campo.resposta;

    if (campo.tipo === 'lista') {
      const opcao = campo.opcoes.find(resp => campo.resposta === resp.codigo.toString());
      this.cdOpcao = isNaN(Number(campo.resposta)) ? null : campo.resposta;
      this.dsRespt = opcao ? opcao.descricao : null;
    }
    if (campo.tipo === 'cep' && this.dsRespt) {
      this.dsRespt = this.dsRespt.replace(/-/g, '');
    }
  }

}
