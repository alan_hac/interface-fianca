export class RespostasRecalculo {
  constructor(
    public pcComissao: number,
    public pcProLabore: number,
    public pcAgravo: number,
    public pcDesconto: number
  ) {}
}
