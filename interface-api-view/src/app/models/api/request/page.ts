export class PageRequest {
  constructor(
    public page?: number,
    public size?: number,
    public start?: string,
    public end?: string,
    public sort?: string,
    public direction?: 'DESC'|'ASC'
  ) { }
}
