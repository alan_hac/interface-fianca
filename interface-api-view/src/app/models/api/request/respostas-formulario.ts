import { Formulario } from '../response/formulario/formulario';
import { Resposta } from './resposta';

export class RespostasFormulario {
  public respostas: Resposta[] = new Array<Resposta>();

  constructor(formulario: Formulario) {
    const campos = formulario.campos;
    Object.keys(campos).forEach(codCampo => this.respostas.push(new Resposta(campos[codCampo])));
  }
}
