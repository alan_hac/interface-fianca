export class SelecaoCalculo {
  constructor(
    public calculoContratado: string,
    public codFormaCobranca: string,
    public codFormaPagamento: string,
    public qtdParcela: number
  ) {}
}
