import { Aba } from './comum/aba';
import { ResponseAPI } from './response-api';
export class DadosCotacaoAPI extends ResponseAPI {
  public abas: Aba[];

  constructor() {
    super();
  }
}
