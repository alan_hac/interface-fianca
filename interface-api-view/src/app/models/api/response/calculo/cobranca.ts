import { Parcela } from "./parcela";

export class Cobranca {
  constructor(
    public vista: Map<string, Parcela>,
    public prazo: Map<string, Parcela>
  ) {}
}
