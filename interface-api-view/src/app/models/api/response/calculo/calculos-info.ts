import { ResponseAPI } from '../response-api';
import { Assistencia } from './assistencia';

export class CalculosInfo extends ResponseAPI {
  constructor(
    public ordemApresentacaoCalculos?: Array<string>,
    public calculos?: Map<string, Assistencia>,
    public calculoContratado?: string,
    public codFormaPagamento?: string,
    public codFormaCobranca?: string,
    public qtdParcela?: number,
    public pcComissao?: number,
    public pcProLabore?: number,
    public pcDesconto?: number,
    public pcAgravo?: number,
    public icPlabo?: boolean,
    public icAlterComis?: boolean,
    public icAlterPlabo?: boolean,
    public icAgrvcComrl?: boolean,
    public icDesctComrl?: boolean
  ) {
    super();
  }
}
