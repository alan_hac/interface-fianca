import { Cobranca } from './cobranca';
export class OpcaoPagamento {
  constructor(
    public codFormaPagamento: string,
    public codFormaCobranca: string,
    public qtdParcela: number,
    public debito: Cobranca,
    public credito: Cobranca,
    public ficha: Cobranca,
    public faturado: Cobranca
  ) {}
}
