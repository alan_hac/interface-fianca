export class Parcela {
  constructor(
    public qtd: number,
    public pcJur: number,
    public vlPremTot: number,
    public vlJurTot: number,
    public vlIofTot: number,
    public vlPremPar: number,
    public vlJurPar: number,
    public vlIofParc: number
  ) {}
}
