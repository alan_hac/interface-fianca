import { OpcaoPagamento } from "./opcao-pagamento";
import { Franquia } from "../formulario/franquias";
import { Diferencial } from "../formulario/diferancial";

export class Assistencia {
  constructor(
    public tpSugestao: string,
    public opcaoAssistencia: string,
    public opcoesPagamento: OpcaoPagamento,
    public sugestaoValorTotal: number,
    public sugestaoPagto: string,
    public sugestaoQtdParc: number,
    public sugestaoJuros: string,
    public sugestaoValorParc: number,
    public sugestaoCobranc: string,
    public descSugestaoPagto: string,
    public descSugestaoCobranc: string,
    public coberturas: Array<Franquia>,
    public diferenciais: Array<Diferencial>,
    public valorLMG: number
  ) {}
}
