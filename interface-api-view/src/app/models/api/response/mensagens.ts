export class Mensagens {
  constructor(
    public danger?: string[],
    public info?: string[],
    public warning?: string[],
    public success?: string[]
  ) {}
}
