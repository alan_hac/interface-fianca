import { Pagamento } from '../formulario/pagamento';
import { ComplementoInfo } from '../complemento/complemento-info';

export class PropostaInfo {
  constructor(
    public pagamento?: Pagamento,
    public segurado?: ComplementoInfo,
    public garantidos?: Array<ComplementoInfo>
  ) {}
}
