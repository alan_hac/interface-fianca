import { Aba } from './comum/aba';

export class CotacaoInfo {
  public sqCdCotac: number;
  public abaInicial: Aba;
  public cdSitucCotac: string;
  public cdNgocoRsvdo: number;
  public dtPrmroCallo: Date;
  public dtVldad: Date;
  public token: string;
  constructor() {}
}
