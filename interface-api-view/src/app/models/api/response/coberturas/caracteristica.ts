export class Caracteristica {
  constructor(public valores: Array<{ codigo: number; descricao: string }>, public valorSelecionado: number) {}
}
