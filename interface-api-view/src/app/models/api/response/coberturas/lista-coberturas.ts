import { ResponseAPI } from '../response-api';
import { Cobertura } from './cobertura';

export class ListaCoberturas extends ResponseAPI {
  constructor(public coberturas?: Array<Cobertura>) {
    super();
  }
}
