import { Caracteristica } from './caracteristica';

export class Cobertura {
  constructor(public codigo: number, public descricao: string, public valorVerba: number, public caracteristica: Caracteristica) { }
}
