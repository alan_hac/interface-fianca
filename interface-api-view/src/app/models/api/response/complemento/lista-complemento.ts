import { ComplementoInfo } from './complemento-info';
import { ResponseAPI } from './../response-api';

export class ListaComplemento extends ResponseAPI {

  constructor(
    public dadosLocacao?: Array<ComplementoInfo>,
    public preenchimentoDadosLocacao?: string,
    public segurado?: ComplementoInfo,
    public garantidos?: Array<ComplementoInfo>
  ) {
    super();
  }
}
