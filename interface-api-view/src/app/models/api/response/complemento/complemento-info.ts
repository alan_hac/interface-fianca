export class ComplementoInfo {
  constructor(
    public idGardo?: number,
    public nome?: string,
    public tipo?: string,
    public documento?: string,
    public preenchimento?: string,
    public pretendentePrincipal?: string
  ) {}
}
