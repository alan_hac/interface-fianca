export class Pagamento {
  constructor(
    public nomeFormaCobranca?: string,
    public qtdParcela?: number,
    public valorJuros?: string,
    public valorParcela?: number,
    public valorTotal?: number
  ) {}
}
