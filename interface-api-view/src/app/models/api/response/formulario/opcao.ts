export class Opcao {
  constructor(
    public codigo: number,
    public descricao: string,
    public questoesDependentes: Array<number>
  ) {}
}
