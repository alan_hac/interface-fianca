export class Dependencia {
  constructor(codigoCampo: number, codigosOpcoes: Array<number>) {}

  get codigoCampo(): number {
    return this.codigoCampo;
  }
  get codigosOpcoes(): Array<number> {
    return this.codigosOpcoes;
  }
}
