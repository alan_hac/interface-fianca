export class Agrupamento {
  constructor(codigo?: number, descricao?: string, codCampos?: Array<number>) { }

  get codigo(): number {
    return this.codigo;
  }

  get descricao(): string {
    return this.descricao;
  }

  get codCampos(): Array<number> {
    return this.codCampos;
  }
}
