import { Opcao } from './opcao';
import { Dependencia } from './dependencia';

export class Campo {
  constructor(
    codigo?: number,
    descricao?: string,
    tipo?: string,
    help?: string,
    multiplasRespostas?: boolean,
    obterCampos?: boolean,
    apresenta?: boolean,
    editavel?: boolean,
    obrigatoria?: boolean,
    dependencia?: Dependencia,
    opcoesDependentes?: boolean,
    tamanho?: string,
    qtDigitos?: number,
    resposta?: string,
    opcoes?: Array<Opcao>
  ) { }

  get codigo(): number {
    return this.codigo;
  }
  get descricao(): string {
    return this.descricao;
  }
  get tipo(): string {
    return this.tipo;
  }
  get help(): string {
    return this.help;
  }
  get multiplasRespostas(): boolean {
    return this.multiplasRespostas;
  }
  get obterCampos(): boolean {
    return this.obterCampos;
  }
  get apresenta(): boolean {
    return this.apresenta;
  }
  get editavel(): boolean {
    return this.editavel;
  }
  set editavel(editavel) {
    this.editavel = editavel;
  }
  get obrigatoria(): boolean {
    return this.obrigatoria;
  }
  get dependencia(): Dependencia {
    return this.dependencia;
  }
  get opcoesDependentes(): boolean {
    return this.opcoesDependentes;
  }
  get tamanho(): string {
    return this.tamanho;
  }
  get qtDigitos(): number {
    return this.qtDigitos;
  }
  get resposta(): string {
    return this.resposta;
  }

  set resposta(resposta: string) {
    this.resposta = resposta;
  }

  get opcoes(): Array<Opcao> {
    return this.opcoes;
  }

  set opcoes(opcoes: Array<Opcao>) {
    this.opcoes = opcoes;
  }
}
