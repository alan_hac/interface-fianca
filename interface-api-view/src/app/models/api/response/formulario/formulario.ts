import { Agrupamento } from "./agrupamento";
import { Campo } from "./campo";

export class Formulario {
  constructor(
    public codigo?: number,
    public descricao?: string,
    public icFormularioSimples?: boolean,
    public ordemApresentacaoAgrupamentos?: Array<number>,
    public agrupamentos: Map<string, Agrupamento> = new Map<string, Agrupamento>(),
    public campos?: Map<string, Campo>
  ) {}
}
