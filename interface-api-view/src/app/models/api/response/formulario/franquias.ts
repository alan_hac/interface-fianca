export class Franquia {
  constructor(
    public codigo: number,
    public descricao: string,
    public valorPremio: number,
    public valorIS: number,
    public valorVerba: number,
    public caracteristica: string,
    public textoFranquia: string,
    public textoFranquiaComplementar: string
  ) {}
}
