export class PretendenteInfo {
  constructor(
    public idPretendente?: number,
    public nome?: string,
    public documento?: string,
    public responderaFinanceiramente?: string,
    public principal?: string,
    public status?: string,
    public statusDocumento?: string
  ) {}
}
