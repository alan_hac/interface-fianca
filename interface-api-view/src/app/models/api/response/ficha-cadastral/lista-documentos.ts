import { ResponseAPI } from './../response-api';
import { DocumentosInfo } from './documento-info';
export class ListaDocumentos extends ResponseAPI {

  constructor(public documentos?: Array<DocumentosInfo>) {
    super();
  }
}
