import { PretendenteInfo } from './pretendente-info';
import { ResponseAPI } from './../response-api';

export class ListaPretendentes extends ResponseAPI {

  constructor(
    public valido?: boolean,
    public solicitarPreenchimentoPretendente?: boolean,
    public cancelarPreenchimentoPretendente?: boolean,
    public pretendentes?: { pretendentes: Array<PretendenteInfo>, dadosEmpresa: PretendenteInfo }) {
    super();
  }
}
