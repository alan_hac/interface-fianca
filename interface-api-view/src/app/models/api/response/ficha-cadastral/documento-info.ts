export class DocumentosInfo {
  constructor(
    public idDocto?: number,
    public dsDocto?: string,
    public cdStatus?: string,
    public dtSolct?: Date,
    public dtIncls?: Date,
    public dtValdc?: Date,
    public nmArqv?: string
  ) {}
}
