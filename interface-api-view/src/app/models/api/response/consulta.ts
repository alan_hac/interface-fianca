import { Campo } from './formulario/campo';

export class Consulta {
  constructor(
    public cotacao?: number,
    public mensagens?: Map<string, Array<string>>,
    public campos?: Array<Campo>
  ) {}
}
