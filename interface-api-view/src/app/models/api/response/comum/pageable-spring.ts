export class PageableSpring {
  constructor(
    public content?: any[],
    public size?: number,
    // tslint:disable-next-line: variable-name
    public number?: number,
    public totalPages?: number,
    public totalElements?: number,
    public numberOfElements?: number,
    public last?: boolean,
    public first?: boolean,
  ) { }
}
