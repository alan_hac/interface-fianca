import { ResponseAPI } from '../response-api';
import { Formulario } from '../formulario/formulario';

export class FormularioAPreencher extends ResponseAPI {
  public formulario?: Formulario;

  constructor() {
    super();
  }
}
