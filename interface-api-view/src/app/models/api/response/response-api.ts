import { CotacaoInfo } from './comum/cotacao-info';
import { Mensagens } from './mensagens';

export class ResponseAPI {
  public cotacaoInfo: CotacaoInfo;
  public mensagens: Mensagens;

  constructor() { }
}
