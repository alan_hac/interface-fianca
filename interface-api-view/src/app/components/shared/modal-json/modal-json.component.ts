import { Component, Input, ElementRef, ViewChild, AfterViewInit } from '@angular/core';

import * as mat from 'materialize-css';

@Component({
  selector: 'app-modal-json',
  templateUrl: './modal-json.component.html',
  styleUrls: ['./modal-json.component.scss']
})
export class ModalJsonComponent implements AfterViewInit {


  @ViewChild('modalJson', { static: true } )
  private modal: ElementRef;
  @ViewChild('tabs', {static: true})
  private tabs: ElementRef;
  @Input()
  public rawJsons: Array<any>;

  public modalJson: mat.Modal;

  constructor() { }

  ngAfterViewInit() {
    this.modalJson = mat.Modal.init(this.modal.nativeElement, { dismissible: false });
    mat.Tabs.init(this.tabs.nativeElement);
  }

  openModal() {
    this.modalJson.open();
  }

  closeModal() {
    this.modalJson.close();
  }

}
