import { Component, OnInit, OnDestroy } from '@angular/core';
import { Mensagens } from 'src/app/models/api/response/mensagens';
import { Subscription } from 'rxjs';
import { MensagensService } from 'src/app/services/cotador/comum/mensagens.service';

import * as $ from 'jquery';

@Component({
  selector: 'app-mensagens',
  templateUrl: './mensagens.component.html',
  styleUrls: ['./mensagens.component.scss']
})
export class MensagensComponent implements OnInit, OnDestroy {

  public mensagens: Mensagens;
  private mensagens$: Subscription;

  constructor(private mensagensService: MensagensService) { }

  ngOnInit() {
    this.mensagens$ = this.mensagensService.mensagensEmitter.subscribe((msgs: Mensagens) => {
      if ((msgs.danger && msgs.danger.length !== 0)
          || (msgs.info && msgs.info.length !== 0)
          || (msgs.success && msgs.success.length !== 0)
          || (msgs.warning && msgs.warning.length !== 0)) {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        $('.modal-content').animate({ scrollTop: 0 }, 'slow');
      }
      this.mensagens = msgs;
    });
  }

  ngOnDestroy() {
    this.mensagens$.unsubscribe();
  }

  get chaves() {
    if (this.mensagens) {
      return Object.keys(this.mensagens);
    }
    return new Array<string>();
  }

  fechar(tipo: string) {
    this.mensagens[tipo] = null;
    this.mensagensService.mensagensEmitter.emit(this.mensagens);
  }

  focarCampoComErro(mensagem: string): void {
    if (mensagem) {
      const label = mensagem.split('\'').length > 2 ? mensagem.split('\'')[1] : '' ;
      if (label) {
        const id = $('label:contains(\'' + label + '\')').attr('for');
        if (id) {
          $('html,body').animate({scrollTop: $('#' + id).offset().top}, 'slow', () => $('#' + id).trigger('focus'));
        }
      }
    }
  }

}
