import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxMaskModule } from 'ngx-mask';
import { NgxCurrencyModule } from 'ngx-currency';

import { MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { MensagensComponent } from './mensagens/mensagens.component';
import { FormularioComponent } from './formulario/formulario.component';
import { CampoComponent } from './formulario/campo/campo.component';
import { ModalJsonComponent } from './modal-json/modal-json.component';


@NgModule({
  declarations: [MensagensComponent, FormularioComponent, CampoComponent, ModalJsonComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule,
    NgxCurrencyModule,
    MatInputModule,
    MatFormFieldModule,
    MatAutocompleteModule,
  ],
  exports: [MensagensComponent, FormularioComponent, CampoComponent, ModalJsonComponent]
})
export class SharedModule { }
