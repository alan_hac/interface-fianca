import { element } from 'protractor';
import { Component, Input, Output, EventEmitter, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Campo } from 'src/app/models/api/response/formulario/campo';
import { Opcao } from 'src/app/models/api/response/formulario/opcao';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

import * as mat from 'materialize-css';
import * as $ from 'jquery';

@Component({
  selector: 'app-campo',
  templateUrl: './campo.component.html',
  styleUrls: ['./campo.component.scss']
})
export class CampoComponent implements OnInit, AfterViewInit {

  @ViewChild('input', { static: false } ) private input: ElementRef;
  @Input() public formGroup: FormGroup;
  @Input() public campo: Campo;
  @Output() public carregaDependentes = new EventEmitter<Campo>();

  public campoCtrl: FormControl;
  public opcoes: Observable<Opcao[]>;

  constructor() { }

  ngOnInit(): void {
    this.campoCtrl =  this.formGroup.get('campo' + this.campo.codigo) as FormControl;
    if (this.campoCtrl) {
      this.campoCtrl.patchValue(this.fieldValue, {emitEvent: false});
      if (this.campo.editavel) {
        this.campoCtrl.enable({emitEvent: false});
      }
    } else {
      this.campoCtrl = new FormControl(this.fieldValue, this.campo.obrigatoria ? Validators.required : {});
      this.formGroup.addControl('campo' + this.campo.codigo, this.campoCtrl);
    }

    if (this.campo.editavel) {
      if (this.campo.tipo === 'lista') {
        this.tratarMudancasLista();
      } else {
        this.campoCtrl.valueChanges.subscribe(v => {
          if (this.campoCtrl.valid) {
            this.setValorFormulario(v.toString());
          }
        });
      }
    } else {
      this.campoCtrl.disable({emitEvent: false, onlySelf: true});
    }
  }

  ngAfterViewInit(): void {
    if (this.campo.tipo === 'data') {
      mat.Datepicker.init($(this.input.nativeElement),
        {
          format: 'dd/mm/yyyy',
          parse: (value: string, format: string) => this.stringToDate(value),
          setDefaultDate: true,
          defaultDate: this.stringToDate(this.campoCtrl.value),
          onSelect: (selectedDate: Date) => { this.campoCtrl.setValue(selectedDate.toLocaleDateString()); },
          autoClose: true,
          container: document.querySelector('body')
      });
    }
  }

  get fieldValue(): string {
    if (this.campo.tipo === 'lista') {
      const opcao = this.campo.opcoes.find(o => o.codigo.toString() === this.campo.resposta);
      return opcao ? opcao.descricao : '';
    }
    return this.campo.resposta;
  }

  private tratarMudancasLista(): void {
    this.opcoes =  this.campoCtrl.valueChanges.pipe(
      startWith(this.fieldValue),
      map(opcao => this.campo.opcoes && opcao && this.campoCtrl.invalid ? this.filterOpcoes(opcao) : this.campo.opcoes.slice()));

    this.campoCtrl.valueChanges.subscribe(c => {
        const opcao = this.campo.opcoes.find(o => o.descricao.toLowerCase() === c.toLowerCase());
        if (opcao) {
          this.setValorFormulario(opcao.codigo.toString());
        } else {
          this.campo.resposta = null;
          this.campoCtrl.setErrors({invalid: true});
        }
      });
  }

  private setValorFormulario(valor: string): void {
    if (this.campo.obterCampos && this.campo.resposta !== valor) {
      this.campo.resposta = valor;
      this.carregaDependentes.emit(this.campo);
    } else {
      this.campo.resposta = valor;
    }
  }

  private filterOpcoes(value: any): Opcao[] {
    const filterValue = value.toString().toLowerCase();
    return this.campo.opcoes.filter(opcao => opcao.descricao.toLowerCase().indexOf(filterValue) === 0);
  }

  private stringToDate(strDate: string): Date {
    const brokenDate = strDate ? strDate.split('/').map(e => Number(e)) : null;
    return brokenDate ? new Date(brokenDate[2], brokenDate[1] - 1, brokenDate[0]) : null;
  }

  get mascara(): string {
    if (this.campo.tipo === 'cpf') {
      return '000.000.000-00';
    } else if (this.campo.tipo === 'cnpj') {
      return '00.000.000/0000-00';
    } else if (this.campo.tipo === 'celular') {
      return '(00) 00000-0000';
    } else if (this.campo.tipo === 'telefone') {
      return '(00) 0000-0000';
    } else if (this.campo.tipo === 'cep') {
      return '00000-000';
    } else if (this.campo.tipo === 'data') {
      return '00/00/0000';
    }
  }

}
