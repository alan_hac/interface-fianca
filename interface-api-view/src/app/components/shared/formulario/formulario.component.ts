import { FormGroup } from '@angular/forms';
import { Component, Input } from '@angular/core';
import { ConsultaService } from 'src/app/services/cotador/consulta.service';
import { Formulario } from 'src/app/models/api/response/formulario/formulario';
import { Agrupamento } from 'src/app/models/api/response/formulario/agrupamento';
import { Campo } from 'src/app/models/api/response/formulario/campo';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent {

  @Input()
  public formulario: Formulario;
  @Input()
  public formGroup: FormGroup;

  constructor(private consultaService: ConsultaService) { }

  get agrupamentos(): Array<Agrupamento> {
    const agrupamentos: Array<Agrupamento> = new Array<Agrupamento>();
    for (const key of this.formulario.ordemApresentacaoAgrupamentos) {
      agrupamentos.push(this.formulario.agrupamentos[key]);
    }
    return agrupamentos;
  }

  public campo(key: string): Campo {
    return this.formulario.campos[key];
  }

  public campos(agrupamento: Agrupamento) {
    const campos = agrupamento.codCampos.map(codCampo => this.formulario.campos[codCampo]);
    return campos.filter(campo => this.apresenta(campo));
  }

  public contemCamposVisiveis(agrupamento: Agrupamento) {
    return agrupamento.codCampos.some(codCampo => this.apresenta(this.formulario.campos[codCampo]));
  }

  public apresenta(campo: Campo): boolean {
    if (!campo.dependencia) {
      return campo.apresenta;
    }
    const dependeDe = this.campo(campo.dependencia.codigoCampo.toString());
    if (!dependeDe.resposta || dependeDe.resposta === '' || dependeDe.resposta === '0') {
      return false;
    }
    let resposta = dependeDe.opcoes[0].codigo.toString();
    if (dependeDe.tipo === 'lista') {
      resposta = dependeDe.resposta;
    }
    return (campo.apresenta && campo.dependencia.codigosOpcoes.find(r => r.toString() === resposta) != null);
  }

  public carregaDependentes(campo: Campo): void {
    this.formGroup.disable( { emitEvent: false });

    this.consultaService.getOpcoes(this.formulario.codigo, campo.codigo, campo.resposta)
      .subscribe(consulta => {
        consulta.campos.filter(c => c).forEach(c =>  this.formulario.campos[c.codigo.toString()] = c);
        this.formGroup.enable( { emitEvent: false } );
    });
  }

  class(codCampo: string): string {
    return 'input-field col ' + this.campo(codCampo).tamanho
      .replace(/col-/gi, '')
      .replace(/xs-6/gi, 's12')
      .replace(/sm-4/gi, 's6')
      .replace(/xs-/gi, 's')
      .replace(/sm-/gi, 's')
      .replace(/md-/gi, 'm')
      .replace(/lg-/gi, 'l');
  }
}
