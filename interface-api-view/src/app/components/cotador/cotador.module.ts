import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlockUIModule } from 'ng-block-ui';

import { MatTooltipModule } from '@angular/material/tooltip';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faCaretRight, faFile, faClock, faCheck, faTimes, faPaperclip, faUserEdit, faUserTimes, faPlus,
  faTrash, faSave, faFolderOpen, faCode, faPercent, faFileInvoiceDollar, faFileSignature, faAngleDown,
  faChevronRight, faFileContract, faExchangeAlt
} from '@fortawesome/free-solid-svg-icons';

library.add(faCaretRight, faFile, faClock, faCheck, faTimes, faPaperclip, faUserEdit, faUserTimes, faPlus,
  faTrash, faSave, faFolderOpen, faCode, faPercent, faFileInvoiceDollar, faFileSignature, faAngleDown,
  faChevronRight, faFileContract, faExchangeAlt);

import { SharedModule } from './../shared/shared.module';
import { CotadorRoutingModule } from './cotador.routing';
import { CotadorComponent } from './cotador.component';
import { DadosBasicosComponent } from './dados-basicos/dados-basicos.component';
import { FichaCadastralComponent } from './ficha-cadastral/ficha-cadastral.component';
import { ModalPretendenteComponent } from './ficha-cadastral/modal-pretendente/modal-pretendente.component';
import { CoberturasComponent } from './coberturas/coberturas.component';
import { NavegacaoGuard } from 'src/app/guard/navegacao.guard';
import { ModalDocumentosComponent } from './ficha-cadastral/modal-documentos/modal-documentos.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { CalculoComponent } from './calculo/calculo.component';
import { ModalCondicoesComerciaisComponent } from './calculo/modal-condicoes-comerciais/modal-condicoes-comerciais.component';
import { CardAssistenciaComponent } from './calculo/card-assistencia/card-assistencia.component';
import { ModalPagamentoComponent } from './calculo/modal-pagamento/modal-pagamento.component';
import { ModalConfirmacaoComponent } from './calculo/modal-confirmacao/modal-confirmacao.component';
import { ComplementoComponent } from './complemento/complemento.component';
import { ModalComplementoComponent } from './complemento/modal-complemento/modal-complemento.component';
import { PropostaComponent } from './proposta/proposta.component';

@NgModule({
  declarations: [
    CotadorComponent,
    DadosBasicosComponent,
    FichaCadastralComponent,
    ModalPretendenteComponent,
    CoberturasComponent,
    ModalDocumentosComponent,
    CalculoComponent,
    ModalCondicoesComerciaisComponent,
    CardAssistenciaComponent,
    ModalPagamentoComponent,
    ModalConfirmacaoComponent,
    ComplementoComponent,
    ModalComplementoComponent,
    PropostaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    BlockUIModule.forRoot(),
    NgxCurrencyModule,
    MatTooltipModule,
    CotadorRoutingModule,
    SharedModule
  ],
  providers: [
    NavegacaoGuard
  ]
})
export class CotadorModule { }
