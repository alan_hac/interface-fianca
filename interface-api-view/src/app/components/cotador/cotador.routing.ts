import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CotadorComponent } from './cotador.component';
import { CotacaoInfoResolver } from 'src/app/resolvers/cotacao-info.resolver';
import { DadosBasicosComponent } from './dados-basicos/dados-basicos.component';
import { FichaCadastralComponent } from './ficha-cadastral/ficha-cadastral.component';
import { CoberturasComponent } from './coberturas/coberturas.component';
import { NavegacaoGuard } from 'src/app/guard/navegacao.guard';
import { CalculoComponent } from './calculo/calculo.component';
import { ComplementoComponent } from './complemento/complemento.component';
import { PropostaComponent } from './proposta/proposta.component';

const routes: Routes = [
  { path: '', component: CotadorComponent, resolve: { cotacaoInfo: CotacaoInfoResolver },
    children: [
        { path: 'dados-item/:cotacao', component: DadosBasicosComponent, canActivate: [NavegacaoGuard] },
        { path: 'ficha-cadastral/:cotacao', component: FichaCadastralComponent, canActivate: [NavegacaoGuard]  },
        { path: 'coberturas/:cotacao', component: CoberturasComponent, canActivate: [NavegacaoGuard]  },
        { path: 'calculo/:cotacao', component: CalculoComponent, canActivate: [NavegacaoGuard]},
        { path: 'complemento/:cotacao', component: ComplementoComponent, canActivate: [NavegacaoGuard]},
        { path: 'proposta/:cotacao', component: PropostaComponent, canActivate: [NavegacaoGuard]}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CotadorRoutingModule {}
