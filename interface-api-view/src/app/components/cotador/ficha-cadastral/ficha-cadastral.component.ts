import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FichaCadastralService } from 'src/app/services/cotador/ficha-cadastral.service';
import { ListaPretendentes } from 'src/app/models/api/response/ficha-cadastral/lista-pretendentes';
import { PretendenteInfo } from 'src/app/models/api/response/ficha-cadastral/pretendente-info';

@Component({
  selector: 'app-ficha-cadastral',
  templateUrl: './ficha-cadastral.component.html',
  styleUrls: ['./ficha-cadastral.component.scss']
})
export class FichaCadastralComponent implements OnInit {

  public dadosEmpresa: PretendenteInfo;
  public pretendentes: Array<PretendenteInfo>;
  public solicitaCAD: boolean;
  public cancelaCAD: boolean;
  private resumoPretendentes: ListaPretendentes;

  constructor(private router: Router, private fichaCadastralService: FichaCadastralService) { }

  ngOnInit() {
    this.carregarListaPretendentes();
  }

  public carregarListaPretendentes() {
    this.fichaCadastralService
      .getResumoPretendentes()
      .subscribe(lista => this.retornoListaPretendentes(lista));
  }

  excluir(idPretendente: number): void {
    this.fichaCadastralService
      .deletePretendente(idPretendente)
      .subscribe(lista => this.retornoListaPretendentes(lista));
  }

  public retornoListaPretendentes(resumoPretendentes: ListaPretendentes) {
    this.resumoPretendentes = resumoPretendentes;
    this.dadosEmpresa = resumoPretendentes.pretendentes.dadosEmpresa;
    this.pretendentes = resumoPretendentes.pretendentes.pretendentes;
    this.solicitaCAD = resumoPretendentes.solicitarPreenchimentoPretendente;
    this.cancelaCAD = resumoPretendentes.cancelarPreenchimentoPretendente;
  }

  validaPretendentes() {
    this.fichaCadastralService.validarPretendentes()
      .subscribe(lista =>
        lista.valido ? this.router.navigate(['/cotador/coberturas/' + lista.cotacaoInfo.sqCdCotac]) : this.retornoListaPretendentes(lista));
  }

  solicitarCAD() {
    this.fichaCadastralService.solicitarCadastroPretendente().subscribe(() => {
        this.cancelaCAD = true;
        this.solicitaCAD = false;
      });
  }

  cancelarCAD() {
    this.fichaCadastralService.solicitarCadastroPretendente().subscribe(() => {
        this.cancelaCAD = false;
        this.solicitaCAD = true;
      });
  }

  reenviarCAD() {
    this.fichaCadastralService.solicitarCadastroPretendente().subscribe();
  }

  statusDocumento(statusDocumento: string): [string, string] {
    if (statusDocumento === 'PEN') {
      return ['fas', 'file'];
    }
    if (statusDocumento === 'ANL') {
      return ['fas', 'clock'];
    }
    if (statusDocumento === 'APR') {
      return ['fas', 'check'];
    }
    if (statusDocumento === 'REP') {
      return ['fas', 'times'];
    }
  }

  tooltipDocumento(statusDocumento: string): string {
    if (statusDocumento === 'PEN') {
      return 'Anexo Pendente';
    }
    if (statusDocumento === 'ANL') {
      return 'Em análise';
    }
    if (statusDocumento === 'APR') {
      return 'Aprovado';
    }
    if (statusDocumento === 'REP') {
      return 'Recusado';
    }
  }

  get rawJson(): Array<any> {
    if (this.resumoPretendentes) {
      return new Array<any>(this.resumoPretendentes);
    }

    return [];
  }

}
