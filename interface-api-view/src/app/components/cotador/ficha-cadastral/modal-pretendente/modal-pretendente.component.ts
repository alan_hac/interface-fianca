import { ListaPretendentes } from './../../../../models/api/response/ficha-cadastral/lista-pretendentes';
import { Component, AfterViewInit, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';

import { FormularioAPreencher } from 'src/app/models/api/response/comum/formulario-a-responder';
import { FichaCadastralService } from 'src/app/services/cotador/ficha-cadastral.service';

import * as mat from 'materialize-css';
import { FormGroup } from '@angular/forms';
import { RespostasFormulario } from 'src/app/models/api/request/respostas-formulario';

@Component({
  selector: 'app-modal-pretendente',
  templateUrl: './modal-pretendente.component.html',
  styleUrls: ['./modal-pretendente.component.scss']
})
export class ModalPretendenteComponent implements AfterViewInit {

  @ViewChild('modalPretendente', { static: true } )
  private modal: ElementRef;

  @Output()
  public callbackListaPretendentes = new EventEmitter<ListaPretendentes>();

  public formGroup = new FormGroup({});
  public modalPretendente: mat.Modal;
  public idPretendente: number;
  public formularioAPreencher: FormularioAPreencher;

  constructor(private fichaCadastralService: FichaCadastralService) { }

  ngAfterViewInit() {
    this.modalPretendente = mat.Modal.init(this.modal.nativeElement, { dismissible: false });
  }

  openModal(idPretendente: number) {
    this.idPretendente = idPretendente;
    this.fichaCadastralService.getDadosPretendente(idPretendente)
      .subscribe(dados => {
        this.formularioAPreencher = dados;
        this.modalPretendente.open();
      }
    );
  }

  closeModal() {
    this.modalPretendente.close();
  }

  salvar() {
    this.fichaCadastralService
      .putPretendente(this.idPretendente, new RespostasFormulario(this.formularioAPreencher.formulario))
      .subscribe(lista => this.retornoPretendenteSalvo(lista));
  }

  salvarEValidar() {
    this.fichaCadastralService
      .postPretendente(this.idPretendente, new RespostasFormulario(this.formularioAPreencher.formulario))
      .subscribe(lista => this.retornoPretendenteSalvo(lista));
  }

  private retornoPretendenteSalvo(lista: ListaPretendentes) {
    this.callbackListaPretendentes.emit(lista);
    this.closeModal();
  }

  get rawJson(): Array<any> {
    if (this.formularioAPreencher) {
      return new Array<any>(this.formularioAPreencher, new RespostasFormulario(this.formularioAPreencher.formulario));
    }

    return [];
  }

}
