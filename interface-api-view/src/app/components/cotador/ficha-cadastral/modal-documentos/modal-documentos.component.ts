import { Component, AfterViewInit, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';

import * as $ from 'jquery';
import * as mat from 'materialize-css';

import { ListaDocumentos } from 'src/app/models/api/response/ficha-cadastral/lista-documentos';
import { FichaCadastralService } from 'src/app/services/cotador/ficha-cadastral.service';
import { ListaPretendentes } from 'src/app/models/api/response/ficha-cadastral/lista-pretendentes';
import { PretendenteInfo } from 'src/app/models/api/response/ficha-cadastral/pretendente-info';
import { HttpEventType, HttpEvent } from '@angular/common/http';

@Component({
  selector: 'app-modal-documentos',
  templateUrl: './modal-documentos.component.html',
  styleUrls: ['./modal-documentos.component.scss']
})
export class ModalDocumentosComponent implements AfterViewInit {

  @ViewChild('modalDocumentos', { static: true } )
  private modal: ElementRef;

  @Output()
  public callbackListaPretendentes = new EventEmitter<ListaPretendentes>();

  public modalDocumentos: mat.Modal;
  public pretendente: PretendenteInfo;
  public listaDocumentos: ListaDocumentos;

  constructor(private fichaCadastralService: FichaCadastralService) { }

  ngAfterViewInit() {
    this.modalDocumentos = mat.Modal.init(this.modal.nativeElement, { dismissible: false });
  }

  openModal(pretendente: PretendenteInfo) {
    this.pretendente = pretendente;
    this.fichaCadastralService.getDadosDocumentosPretendente(pretendente.idPretendente)
      .subscribe(lista => {
        this.listaDocumentos = lista;
        this.modalDocumentos.open();
      });
  }

  closeModal() {
    this.fichaCadastralService.getResumoPretendentes().subscribe(lista => {
      this.callbackListaPretendentes.emit(lista);
      this.modalDocumentos.close();
    });
  }

  documentoAnexado(idDocto: number, inputEvent, loadElement: ElementRef) {
    this.fichaCadastralService.uploadDocumento(idDocto, inputEvent.target.files[0])
      .subscribe((httpEvent: HttpEvent<any>) => {
        if (httpEvent.type === HttpEventType.Sent) {
          $(loadElement).show();
        } else if (HttpEventType.UploadProgress === httpEvent.type) {
          $(loadElement).children('.determinate').css('width', ((httpEvent.loaded * 100) / httpEvent.total ) + '%');
        } else if (httpEvent.type === HttpEventType.Response) {
          $(loadElement).hide();
          this.listaDocumentos = httpEvent.body;
          if (!this.listaDocumentos.documentos.some(d => d.cdStatus === 'PEN' || d.cdStatus === 'REP')) {
            this.fichaCadastralService.getResumoPretendentes().subscribe(lista => this.callbackListaPretendentes.emit(lista));
          }
        }
      });
  }

  abrirDocumento(idDocto: number, nomeArquivo: string) {
    this.fichaCadastralService.downloadDocumento(idDocto, nomeArquivo);
  }

  statusDocumento(statusDocumento: string): [string, string] {
    if (statusDocumento === 'PEN') {
      return ['fas', 'file'];
    }
    if (statusDocumento === 'ANL') {
      return ['fas', 'clock'];
    }
    if (statusDocumento === 'APR') {
      return ['fas', 'check'];
    }
    if (statusDocumento === 'REP') {
      return ['fas', 'times'];
    }
  }

  tooltipDocumento(statusDocumento: string): string {
    if (statusDocumento === 'PEN') {
      return 'Anexo Pendente';
    }
    if (statusDocumento === 'ANL') {
      return 'Em análise';
    }
    if (statusDocumento === 'APR') {
      return 'Aprovado';
    }
    if (statusDocumento === 'REP') {
      return 'Recusado';
    }
  }

  get rawJson(): Array<any> {
    if (this.listaDocumentos) {
      return new Array<any>(this.listaDocumentos);
    }

    return [];
  }


}
