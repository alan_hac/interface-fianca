import { Component, OnInit } from '@angular/core';
import { ListaComplemento } from 'src/app/models/api/response/complemento/lista-complemento';
import { ComplementoService } from 'src/app/services/cotador/complemento.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-complemento',
  templateUrl: './complemento.component.html',
  styleUrls: ['./complemento.component.scss']
})
export class ComplementoComponent implements OnInit {

  public listaComplemento: ListaComplemento;

  constructor(private complementoService: ComplementoService, private router: Router) { }

  ngOnInit() {
    this.complementoService.listDadosComplemento().subscribe(lista => (this.listaComplemento = lista));
  }

  public carregarListaComplemento(listaComplemento: ListaComplemento) {
    this.listaComplemento = listaComplemento;
  }

  public confirmarProposta() {
    this.complementoService.validateComplemento()
      .subscribe(dados => this.router.navigate(['/cotador/proposta/' + dados.cotacaoInfo.sqCdCotac]));
  }

  get rawJson(): Array<any> {
    if (this.listaComplemento) {
      return new Array<any>(this.listaComplemento);
    }
    return [];
  }

}
