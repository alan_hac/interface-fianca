import { ComplementoService } from 'src/app/services/cotador/complemento.service';
import { Component, AfterViewInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import * as mat from 'materialize-css';
import { FormularioAPreencher } from 'src/app/models/api/response/comum/formulario-a-responder';
import { Observable } from 'rxjs';
import { RespostasFormulario } from 'src/app/models/api/request/respostas-formulario';
import { ListaComplemento } from 'src/app/models/api/response/complemento/lista-complemento';

@Component({
  selector: 'app-modal-complemento',
  templateUrl: './modal-complemento.component.html',
  styleUrls: ['./modal-complemento.component.scss']
})
export class ModalComplementoComponent implements AfterViewInit {

  @ViewChild('modalComplemento', { static: true } )
  private modal: ElementRef;

  @Output()
  public carregarListaComplemento = new EventEmitter<ListaComplemento>();

  public formGroup = new FormGroup({});
  public modalComplemento: mat.Modal;
  public idGarantido: number;
  public tpFormulario: string;
  public formularioAPreencher: FormularioAPreencher;

  constructor(private complementoService: ComplementoService) { }

  ngAfterViewInit() {
    this.modalComplemento = mat.Modal.init(this.modal.nativeElement, { dismissible: false });
  }

  openModal(tpFormulario: string, idGarantido: number) {
    this.idGarantido = idGarantido;
    this.tpFormulario = tpFormulario;
    let $formularioRequest: Observable<FormularioAPreencher>;

    if (this.tpFormulario === 'Locacao') {
      $formularioRequest = this.complementoService.getDadosLocacao();
    } else if (this.tpFormulario === 'Segurado') {
      $formularioRequest = this.complementoService.getDadosSegurado();
    } else {
      $formularioRequest = this.complementoService.getDadosPretendente(idGarantido);
    }

    $formularioRequest .subscribe(dados => { this.formularioAPreencher = dados; this.modalComplemento.open(); } );
  }

  closeModal() {
    this.modalComplemento.close();
  }

  salvar() {
    let $formularioRequest: Observable<ListaComplemento>;
    if (this.tpFormulario === 'Locacao') {
      $formularioRequest = this.complementoService.postDadosLocacao( new RespostasFormulario(this.formularioAPreencher.formulario) );
    } else if (this.tpFormulario === 'Segurado') {
      $formularioRequest = this.complementoService.postDadosSegurado( new RespostasFormulario(this.formularioAPreencher.formulario) );
    } else {
      $formularioRequest = this.complementoService
        .postDadosPretendente(this.idGarantido, new RespostasFormulario(this.formularioAPreencher.formulario));
    }

    $formularioRequest.subscribe(lista => { this.carregarListaComplemento.emit(lista); this.closeModal(); });
  }

  get rawJson(): Array<any> {
    if (this.formularioAPreencher) {
      return new Array<any>(this.formularioAPreencher, new RespostasFormulario(this.formularioAPreencher.formulario));
    }
    return [];
  }

}
