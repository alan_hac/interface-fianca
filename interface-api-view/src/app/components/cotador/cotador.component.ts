import { CotacaoInfoService } from 'src/app/services/cotador/comum/cotacao-info.service';
import { Component, OnInit } from '@angular/core';
import { Aba } from 'src/app/models/api/response/comum/aba';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cotador',
  templateUrl: './cotador.component.html',
  styleUrls: ['./cotador.component.scss']
})
export class CotadorComponent implements OnInit {

  constructor(private cotacaoInfoService: CotacaoInfoService, private router: Router) { }

  ngOnInit() { }

  get situacao(): string {
    return this.cotacaoInfoService.situacao;
  }

  get cdNgocoRsvdo(): number {
    return this.cotacaoInfoService.cotacaoInfo.cdNgocoRsvdo;
  }

  get sqCdCotac(): number {
    return this.cotacaoInfoService.cotacaoInfo.sqCdCotac;
  }

  get dtPrmroCallo(): Date {
    return this.cotacaoInfoService.cotacaoInfo.dtPrmroCallo;
  }

  get dtVldad(): Date {
    return this.cotacaoInfoService.cotacaoInfo.dtVldad;
  }

  get abaAtual(): Aba {
    const routes = this.router.url.split('/');
    return this.abas.find(a => a.rota.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase() === routes[routes.length - 2]);
  }

  get abas(): Aba[] {
    return this.cotacaoInfoService.abasNavegacao;
  }

  get abasMobile(): Aba[] {
    return this.abas.filter(s => s.id === this.abaAtual.id || s.id === this.abaAtual.id + 1 || s.id === this.abaAtual.id - 1);
  }

  ativo(id: number): boolean {
    const abaAtual = this.abaAtual;
    return abaAtual && id <= abaAtual.id;
  }

  rota(rotaOriginal: string): string {
    return rotaOriginal.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase();
  }

  barSize(isMobile: boolean): number {
    const abaAtual = this.abaAtual;
    if (!abaAtual) {
      return 0;
    }
    if (isMobile) {
      const tamanho = abaAtual.id === 1 ? 2 : 3;
      return (100 / tamanho) * (abaAtual.id);
    }
    return (100 / this.abas.length) * (abaAtual.id);
  }

}
