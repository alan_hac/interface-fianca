import { Component, ViewChild, ElementRef, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { Assistencia } from 'src/app/models/api/response/calculo/assistencia';
import { Parcela } from 'src/app/models/api/response/calculo/parcela';
import { Cobranca } from 'src/app/models/api/response/calculo/cobranca';
import { SelecaoCalculo } from 'src/app/models/api/request/selecao-calculo';

import * as mat from 'materialize-css';
import * as $ from 'jquery';

@Component({
  selector: 'app-modal-pagamento',
  templateUrl: './modal-pagamento.component.html',
  styleUrls: ['./modal-pagamento.component.scss']
})
export class ModalPagamentoComponent implements AfterViewInit {


  @Output() public salvarPagamento = new EventEmitter<SelecaoCalculo>();
  @ViewChild('modalPagamento', { static: true } ) private modal: ElementRef;

  public assistencia: Assistencia;
  public modalPagamento: mat.Modal;
  public selects: mat.FormSelect[];

  public formGroup = new FormGroup({
    calculoContratado: new FormControl('', {}),
    codFormaCobranca: new FormControl('', {}),
    codFormaPagamento: new FormControl('', {}),
    qtdParcela: new FormControl('', {}),
  });

  constructor() { }

  ngAfterViewInit() {
    this.modalPagamento = mat.Modal.init(this.modal.nativeElement, { dismissible: false });
  }

  openModal(assistencia: Assistencia) {
    this.assistencia = assistencia;
    if (assistencia.sugestaoCobranc) {
      this.formGroup.get('codFormaCobranca').setValue(assistencia.sugestaoCobranc.slice(0, 1));
    }
    this.formGroup.get('codFormaPagamento').setValue(assistencia.sugestaoPagto);
    this.formGroup.get('qtdParcela').setValue(assistencia.sugestaoQtdParc);
    this.formGroup.get('calculoContratado').setValue(assistencia.tpSugestao);
    this.modalPagamento.open();
    setTimeout(() => mat.FormSelect.init($('select')));
  }

  closeModal() {
    this.modalPagamento.close();
  }

  get cobrancaSelecionada(): Cobranca {
    if (this.formGroup.get('codFormaCobranca').value === 'C') {
      return this.assistencia.opcoesPagamento.credito;
    }
    if (this.formGroup.get('codFormaCobranca').value === 'D') {
      return this.assistencia.opcoesPagamento.debito;
    }
    if (this.formGroup.get('codFormaCobranca').value === 'F') {
      return this.assistencia.opcoesPagamento.ficha;
    }
    if (this.formGroup.get('codFormaCobranca').value === 'T') {
      return this.assistencia.opcoesPagamento.faturado;
    }
    return null;
  }

  get parcelas(): Array<Parcela> {
    const cobranca = this.cobrancaSelecionada;
    if (cobranca) {
      if (this.formGroup.get('codFormaPagamento').value === 'V') {
        return Object.values(cobranca.vista);
      }
      if (this.formGroup.get('codFormaPagamento').value === 'P') {
        return Object.values(cobranca.prazo);
      }
    }
    return [];
  }

  selecionar() {

  }
}
