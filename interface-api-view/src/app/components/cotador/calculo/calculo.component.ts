import { Router } from '@angular/router';
import { ImpressaoService } from './../../../services/cotador/impressao.service';
import { Component, OnInit } from '@angular/core';
import { CalculosInfo } from 'src/app/models/api/response/calculo/calculos-info';
import { CalculoService } from 'src/app/services/cotador/calculo.service';
import { RespostasRecalculo } from 'src/app/models/api/request/respostas-recalculo';
import { SelecaoCalculo } from 'src/app/models/api/request/selecao-calculo';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.scss']
})
export class CalculoComponent implements OnInit {

  public calculos: CalculosInfo;

  constructor(private calculoService: CalculoService,
              private impressaoService: ImpressaoService,
              private router: Router) { }

  ngOnInit() {
    this.calculoService.getCalculo().subscribe(response => {
      this.calculos = response;
    });
  }

  recalculo(calculos: CalculosInfo) {
    this.calculos = calculos;
  }

  salvarCalculo(calculoEPagamentoSelecionado: SelecaoCalculo) {
    this.calculoService.postCalculo(calculoEPagamentoSelecionado)
      .subscribe(res => this.router.navigate(['/cotador/complemento/' + this.calculos.cotacaoInfo.sqCdCotac]));
  }

  get assistencias() {
    return this.calculos.ordemApresentacaoCalculos.map(a => this.calculos.calculos[a]);
  }

  get condicoesComerciais() {
    return new RespostasRecalculo(this.calculos.pcComissao, this.calculos.pcProLabore, this.calculos.pcAgravo, this.calculos.pcDesconto);
  }

  imprimirCotacao() {
    this.impressaoService.getArquivoCotacao();
  }

  imprimirParcer() {
    this.impressaoService.getArquivoCartaParcer();
  }

  imprimirFicha() {
    this.impressaoService.getArquivoFichaCadastral();
  }

  get rawJson(): Array<any> {
    if (this.calculos) {
      return new Array<any>(this.calculos, new SelecaoCalculo('N', 'C', 'V', 1));
    }
    return [];
  }
}
