import { Assistencia } from './../../../../models/api/response/calculo/assistencia';
import { Component, Input, ViewChild, ElementRef, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { RespostasRecalculo } from 'src/app/models/api/request/respostas-recalculo';

import * as mat from 'materialize-css';
import * as $ from 'jquery';

@Component({
  selector: 'app-card-assistencia',
  templateUrl: './card-assistencia.component.html',
  styleUrls: ['./card-assistencia.component.scss']
})
export class CardAssistenciaComponent implements AfterViewInit {

  @Input() public assistencia: Assistencia;
  @Input() public condicoesComerciais: RespostasRecalculo;
  @Output() public comparar = new EventEmitter<{check: boolean, tpSugestao: string}>();
  @Output() public selecionado = new EventEmitter<Assistencia>();

  constructor() { }

  ngAfterViewInit() {
    mat.Collapsible.init($('.collapsible'));
  }

}
