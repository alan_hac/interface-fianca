import { CalculoService } from './../../../../services/cotador/calculo.service';
import { CalculosInfo } from './../../../../models/api/response/calculo/calculos-info';
import { Component, OnInit, Input, ElementRef, ViewChild, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, AbstractControl } from '@angular/forms';

import * as mat from 'materialize-css';

@Component({
  selector: 'app-modal-condicoes-comerciais',
  templateUrl: './modal-condicoes-comerciais.component.html',
  styleUrls: ['./modal-condicoes-comerciais.component.scss']
})
export class ModalCondicoesComerciaisComponent implements OnInit, AfterViewInit {

  @Input()
  public calculos: CalculosInfo;
  @Output()
  private recalculo = new EventEmitter<CalculosInfo>();


  @ViewChild('modalCondicoesComerciais', { static: true } )
  private modal: ElementRef;

  public formGroup = new FormGroup({
    pcComissao: new FormControl('', {}),
    pcProLabore: new FormControl('', {}),
    pcAgravo: new FormControl('', {}),
    pcDesconto: new FormControl('', {}),
  });
  public modalCondicoesComerciais: mat.Modal;

  constructor(private calculoService: CalculoService) { }

  ngOnInit() {
    this.formGroup.get('pcAgravo').valueChanges.subscribe(val => this.consistenciaAgravoDesconto(this.formGroup.get('pcDesconto'), val));
    this.formGroup.get('pcDesconto').valueChanges.subscribe(val => this.consistenciaAgravoDesconto(this.formGroup.get('pcAgravo'), val));
  }

  ngAfterViewInit() {
    this.modalCondicoesComerciais = mat.Modal.init(this.modal.nativeElement, { dismissible: false });
  }

  openModal() {
    this.formGroup.get('pcComissao').setValue(this.calculos.pcComissao);
    this.formGroup.get('pcProLabore').setValue(this.calculos.pcProLabore);
    this.formGroup.get('pcAgravo').setValue(this.calculos.pcAgravo);
    this.formGroup.get('pcDesconto').setValue(this.calculos.pcDesconto);
    this.modalCondicoesComerciais.open();
  }

  closeModal() {
    this.modalCondicoesComerciais.close();
  }

  recalcular() {
    this.calculoService.recalcular(this.formGroup.value)
      .subscribe(calculos => {
        this.recalculo.emit(calculos);
        this.closeModal();
      });
  }

  private consistenciaAgravoDesconto(descAgrav: AbstractControl, valor: string) {
    if (valor) {
      descAgrav.setValue(0);
    }
  }

  get rawJson(): Array<any> {
    if (this.calculos) {
      return new Array<any>(this.calculos, this.formGroup.value);
    }

    return [];
  }

}
