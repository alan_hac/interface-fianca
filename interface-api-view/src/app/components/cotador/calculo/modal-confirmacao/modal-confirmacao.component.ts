import { CotacaoInfoService } from './../../../../services/cotador/comum/cotacao-info.service';
import { Component, AfterViewInit, Output, EventEmitter, ElementRef, ViewChild, Input } from '@angular/core';

import * as mat from 'materialize-css';
import { SelecaoCalculo } from 'src/app/models/api/request/selecao-calculo';
import { CalculosInfo } from 'src/app/models/api/response/calculo/calculos-info';
import { Cobranca } from 'src/app/models/api/response/calculo/cobranca';
import { Parcela } from 'src/app/models/api/response/calculo/parcela';
import { OpcaoPagamento } from 'src/app/models/api/response/calculo/opcao-pagamento';

@Component({
  selector: 'app-modal-confirmacao',
  templateUrl: './modal-confirmacao.component.html',
  styleUrls: ['./modal-confirmacao.component.scss']
})
export class ModalConfirmacaoComponent implements AfterViewInit {

  @Input() public calculosInfo: CalculosInfo;
  @Output() public salvarCalculo = new EventEmitter<SelecaoCalculo>();
  @ViewChild('modalPagamento', { static: true } ) private modal: ElementRef;

  public modalConfirmacao: mat.Modal;
  public calculoEPagamento: SelecaoCalculo;
  private parcelaSelecionada: Parcela;

  constructor() { }

  ngAfterViewInit() {
    this.modalConfirmacao = mat.Modal.init(this.modal.nativeElement, { dismissible: false });
  }

  openModal(calculo: SelecaoCalculo) {
    this.calculoEPagamento = calculo;
    this.parcelaSelecionada = this.getParcelaSelecionada();
    this.modalConfirmacao.open();
  }

  closeModal() {
    this.modalConfirmacao.close();
  }

  get cotacao() {
    return this.calculosInfo.cotacaoInfo.sqCdCotac;
  }

  get assistencia() {
    return this.calculosInfo.calculos[this.calculoEPagamento.calculoContratado].opcaoAssistencia;
  }

  get vlPremPar() {
    return this.parcelaSelecionada.vlPremPar;
  }

  get vlPremTot() {
    return this.parcelaSelecionada.vlPremTot;
  }

  private getParcelaSelecionada(): Parcela {
    const opcoes = this.calculosInfo.calculos[this.calculoEPagamento.calculoContratado].opcoesPagamento;
    let opcaoPagamento: Cobranca;

    if (this.calculoEPagamento.codFormaCobranca === 'C') {
      opcaoPagamento = opcoes.credito;
    } else if (this.calculoEPagamento.codFormaCobranca === 'D') {
      opcaoPagamento = opcoes.debito;
    } else if (this.calculoEPagamento.codFormaCobranca === 'F') {
      opcaoPagamento = opcoes.ficha;
    } else if (this.calculoEPagamento.codFormaCobranca === 'T') {
      opcaoPagamento = opcoes.faturado;
    } else {
      return null;
    }

    if (this.calculoEPagamento.codFormaPagamento === 'V') {
      return opcaoPagamento.vista[this.calculoEPagamento.qtdParcela];
    }
    if (this.calculoEPagamento.codFormaPagamento === 'P') {
      return opcaoPagamento.prazo[this.calculoEPagamento.qtdParcela];
    }

  }



}
