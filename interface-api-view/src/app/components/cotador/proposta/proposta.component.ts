import { Component, OnInit } from '@angular/core';

import { PropostaInfo } from 'src/app/models/api/response/proposta/proposta-info';
import { PropostaService } from 'src/app/services/cotador/proposta.service';
import { ImpressaoService } from 'src/app/services/cotador/impressao.service';
import { CotacaoInfoService } from 'src/app/services/cotador/comum/cotacao-info.service';

@Component({
  selector: 'app-proposta',
  templateUrl: './proposta.component.html',
  styleUrls: ['./proposta.component.scss']
})
export class PropostaComponent implements OnInit {

  public resumoProposta: PropostaInfo;

  constructor(private cotacaoInfoService: CotacaoInfoService,
              private propostaService: PropostaService,
              private impressaoservice: ImpressaoService) {}

  ngOnInit() {
    this.propostaService.getResumoProposta().subscribe(response => (this.resumoProposta = response));
  }

  contratar() {
    this.propostaService.contratar().subscribe();
  }

  imprimirProposta() {
    this.impressaoservice.getArquivoProposta();
  }

  get contratado(): boolean {
    return this.cotacaoInfoService.cotacaoInfo.cdSitucCotac === 'TRA';
  }

  get rawJson(): Array<any> {
    if (this.resumoProposta) {
      return new Array<any>(this.resumoProposta);
    }
    return [];
  }

}
