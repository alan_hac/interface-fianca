import { RespostasFormulario } from './../../../models/api/request/respostas-formulario';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormularioAPreencher } from 'src/app/models/api/response/comum/formulario-a-responder';
import { DadosBasicosService } from 'src/app/services/cotador/dados-basicos.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dados-basicos',
  templateUrl: './dados-basicos.component.html',
  styleUrls: ['./dados-basicos.component.scss']
})
export class DadosBasicosComponent implements OnInit {

  public formularioAPreencher: FormularioAPreencher;
  public formGroup = new FormGroup({});

  constructor(private dadosBasicosService: DadosBasicosService, private route: Router) { }

  ngOnInit(): void {
    this.dadosBasicosService.getDadosBasicos().subscribe(dados => (this.formularioAPreencher = dados));
  }

  salvar() {
    this.dadosBasicosService
      .postDadosBasicos(new RespostasFormulario(this.formularioAPreencher.formulario))
      .subscribe(dados => this.route.navigate(['/cotador/ficha-cadastral/' + dados.cotacaoInfo.sqCdCotac]));
  }

  get rawJson(): Array<any> {
    if (this.formularioAPreencher) {
      return new Array<any>(this.formularioAPreencher, new RespostasFormulario(this.formularioAPreencher.formulario));
    }

    return [];
  }
}
