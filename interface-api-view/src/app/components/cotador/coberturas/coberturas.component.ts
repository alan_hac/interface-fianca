import { ListaCoberturas } from 'src/app/models/api/response/coberturas/lista-coberturas';
import { RespostasCoberturas } from './../../../models/api/request/respostas-coberturas';
import { FormularioAPreencher } from './../../../models/api/response/comum/formulario-a-responder';
import { CoberturasService } from 'src/app/services/cotador/coberturas.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { RespostasFormulario } from 'src/app/models/api/request/respostas-formulario';
import { RespostaCobertura } from 'src/app/models/api/request/resposta-cobertura';
import { Cobertura } from 'src/app/models/api/response/coberturas/cobertura';

@Component({
  selector: 'app-coberturas',
  templateUrl: './coberturas.component.html',
  styleUrls: ['./coberturas.component.scss']
})
export class CoberturasComponent implements OnInit {

  public formGroup = new FormGroup({});
  public formGroupCoberturas: FormGroup;
  public formularioLocacao: FormularioAPreencher;
  public formularioCoberturas: ListaCoberturas;

  constructor(private coberturasService: CoberturasService, private router: Router) { }

  ngOnInit() {
    this.coberturasService.getDadosLocacao().subscribe(formularioLocacao => this.formularioLocacao = formularioLocacao);
  }

  obterCoberturas() {
    this.coberturasService.postDadosLocacao(new RespostasFormulario(this.formularioLocacao.formulario))
      .subscribe(formularioCoberturas => {
        this.formGroupCoberturas = new FormGroup({});
        formularioCoberturas.coberturas.forEach(cob => {
          const cobCtrl = new FormControl('', {});
          if (cob.caracteristica) {
            cobCtrl.setValue(cob.caracteristica.valorSelecionado
              && cob.caracteristica.valores[1].codigo === cob.caracteristica.valorSelecionado);
            cobCtrl.valueChanges.subscribe(val =>
              cob.caracteristica.valorSelecionado = (val ? cob.caracteristica.valores[1].codigo : cob.caracteristica.valores[0].codigo));
          } else {
            cobCtrl.setValue(cob.valorVerba ? cob.valorVerba : 0);
            cobCtrl.valueChanges.subscribe(val => cob.valorVerba = val);
          }
          this.formGroupCoberturas.addControl(cob.codigo.toString(), cobCtrl);
        });
        this.formularioCoberturas = formularioCoberturas;
      });
  }

  salvar() {
    const respostasCoberturas = new RespostasCoberturas(
      this.formularioCoberturas.coberturas.map(cob => cob.caracteristica ?
        new RespostaCobertura(cob.codigo, null, cob.caracteristica.valorSelecionado) :
        new RespostaCobertura(cob.codigo, cob.valorVerba ? cob.valorVerba : 0, null))
    );

    this.coberturasService.postDadosCoberturas(respostasCoberturas)
      .subscribe(dados => this.router.navigate(['/cotador/calculo/' + dados.cotacaoInfo.sqCdCotac]));
  }

  get coberturas(): Array<Cobertura> {
    return this.formularioCoberturas.coberturas.filter(cob => !cob.caracteristica);
  }

  get coberturasAdicionais(): Array<Cobertura> {
    return this.formularioCoberturas.coberturas.filter(cob => cob.caracteristica);
  }

  get rawJson(): Array<any> {
    if (this.formularioCoberturas){
      return new Array<any>(this.formularioCoberturas, new RespostasCoberturas(
        this.formularioCoberturas.coberturas.map(cob => cob.caracteristica ?
          new RespostaCobertura(cob.codigo, null, cob.caracteristica.valorSelecionado) :
          new RespostaCobertura(cob.codigo, cob.valorVerba ? cob.valorVerba : 0, null))
      ));
    } else if (this.formularioLocacao) {
      return new Array<any>(this.formularioLocacao, new RespostasFormulario(this.formularioLocacao.formulario));
    }

    return [];
  }
}
