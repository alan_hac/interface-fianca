import { Component, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { catchError } from 'rxjs/operators';

import { LoginService } from 'src/app/services/login/login.service';
import { CotacaoService } from 'src/app/services/cotador/cotacao.service';

import * as $ from 'jquery';
import * as mat from 'materialize-css';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements AfterViewInit {

  constructor(private loginService: LoginService,
              private cotacaoService: CotacaoService,
              private router: Router) { }

  public loginForm = new FormGroup({
    corretor: new FormControl(this.loginService.login.corretor, [Validators.required]),
    cpfUsuario: new FormControl(this.loginService.login.cpfUsuario, [Validators.required]),
    parceiro: new FormControl(this.loginService.login.parceiro),
    senha: new FormControl(this.loginService.login.senha, [Validators.required])
  });

  ngAfterViewInit() {
    mat.updateTextFields();
  }

  public logar(): void {
    this.loginService.login = this.loginForm.value;
    if (this.loginService.isLogado()) {
      this.cotacaoService.listaCotacoes(null)
        .pipe(catchError(err => {
          this.loginService.invalidaLogin();
          this.loginForm.controls.senha.setValue('');
          return err;
        }))
        .subscribe(res => this.router.navigate(['/home']));
    }
  }

}
