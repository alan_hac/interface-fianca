import { ImpressaoService } from '../../services/cotador/impressao.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { CotacaoService } from 'src/app/services/cotador/cotacao.service';
import { CotacaoInfoService } from 'src/app/services/cotador/comum/cotacao-info.service';
import { PageableSpring } from 'src/app/models/api/response/comum/pageable-spring';

import * as $ from 'jquery';
import * as mat from 'materialize-css';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit, OnInit {

  @ViewChild('tabs', {static: true}) private tabs: ElementRef;
  @ViewChild('size', {static: true}) private size: ElementRef;

  public novaCotacaoForm = new FormGroup({
    tpPessoa: new FormControl('', [Validators.required]),
    icResidencial: new FormControl('', [Validators.required]),
  });

  public editarCotacaoForm = new FormGroup({
    sqCdCotac: new FormControl('', { validators: Validators.required })
  });

  public listarCotacoesForm = new FormGroup({
    start: new FormControl(
      this.datePipe.transform(new Date(new Date().getTime() - 24 * 60 * 60 * 1000 * 15), 'dd/MM/yyyy'),
      { validators: Validators.required }
    ),
    end: new FormControl(this.datePipe.transform(new Date(), 'dd/MM/yyyy'), { validators: Validators.required }),
    page: new FormControl('1', { validators: Validators.required }),
    size: new FormControl('5', { validators: Validators.required }),
    sort: new FormControl('sqCdCotac,DESC', { validators: Validators.required })
  });

  public listaCotacoes: PageableSpring;

  constructor(private cotacaoService: CotacaoService,
              private cotacaoInfoService: CotacaoInfoService,
              private impressaoService: ImpressaoService,
              private datePipe: DatePipe,
              private router: Router) { }

  ngOnInit() { }

  ngAfterViewInit() {
    mat.Tabs.init(this.tabs.nativeElement);
    mat.FormSelect.init(this.size.nativeElement);
    $('.datepicker').each((i, el) => {
      const formControl = this.listarCotacoesForm.get(el.id);
      mat.Datepicker.init(el,
        {
          format: 'dd/mm/yyyy',
          parse: (value: string, format: string) => this.stringToDate(value),
          setDefaultDate: true,
          defaultDate: this.stringToDate(formControl.value),
          onSelect: (selectedDate: Date) => { formControl.patchValue(selectedDate.toLocaleDateString()); },
      });
    });
  }

  private stringToDate(strDate: string): Date {
    const brokenDate = strDate ? strDate.split('/').map(e => Number(e)) : null;
    return brokenDate ? new Date(brokenDate[2], brokenDate[1] - 1, brokenDate[0]) : new Date();
  }

  novaCotacao() {
    this.cotacaoService.obterNovaCotacao(this.novaCotacaoForm.get('tpPessoa').value, this.novaCotacaoForm.get('icResidencial').value)
      .subscribe(res => this.redirecionaCotador());
  }

  editarCotacao() {
    this.cotacaoService.obterCotacaoExistente(this.editarCotacaoForm.get('sqCdCotac').value)
      .subscribe(res => this.redirecionaCotador());
  }

  pesquisarCotacoes() {
    this.listarCotacoes(0);
  }

  listarCotacoes(page: number) {
    this.listarCotacoesForm.get('page').setValue(page);
    this.cotacaoService.listaCotacoes(this.listarCotacoesForm.value).subscribe(lista => this.listaCotacoes = lista);
  }

  apoliceCompleta(cotacao) {
    this.cotacaoService
      .obterCotacaoExistente(cotacao)
      .subscribe(() => this.impressaoService.getArquivoApoliceCompleta());
  }

  apoliceResumida(cotacao) {
    this.cotacaoService
      .obterCotacaoExistente(cotacao)
      .subscribe(() => this.impressaoService.getArquivoApoliceResumida());
  }

  cartaoSegurado(cotacao) {
    this.cotacaoService
      .obterCotacaoExistente(cotacao)
      .subscribe(() => this.impressaoService.getArquivoCartaoSegurado());
  }

  cartaoGarantido(cotacao) {
    this.cotacaoService
      .obterCotacaoExistente(cotacao)
      .subscribe(() => this.impressaoService.getArquivoCartaoGarantido());
  }

  private redirecionaCotador() {
    this.router.navigate([this.cotacaoInfoService.rotaAtual]);
  }

  get totalPaginas(): Array<{page: number, active: boolean}> {
    const paginas = new Array<{page: number, active: boolean}>();
    for (let index = 0; index < this.listaCotacoes.totalPages; index++) {
      paginas.push({ page: (index), active: this.listaCotacoes.number === index });
    }
    return paginas;
  }

}
