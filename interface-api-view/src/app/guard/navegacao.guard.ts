import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { CotacaoInfoService } from '../services/cotador/comum/cotacao-info.service';
import { Mensagens } from '../models/api/response/mensagens';
import { MensagensService } from '../services/cotador/comum/mensagens.service';
import { Aba } from '../models/api/response/comum/aba';
import { CotacaoService } from '../services/cotador/cotacao.service';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class NavegacaoGuard implements CanActivate {

  constructor(private cotacaoService: CotacaoService,
              private cotacaoInfoService: CotacaoInfoService,
              private mensagensService: MensagensService,
              private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> {
    const sqCdCotac =  route.url[route.url.length - 1].path;
    const cotacaoInfo = this.cotacaoInfoService.cotacaoInfo;
    const mensagemNavegacao = new Mensagens();
    const abaDestino = this.cotacaoInfoService.abasNavegacao.find(
      aba => aba.rota.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase() === route.url[0].path
    );

    if (!cotacaoInfo) {
      return this.cotacaoService
        .obterCotacaoExistente(Number(sqCdCotac))
        .pipe(map(dados => {
          this.router.navigate([
            '/cotador/' + dados.cotacaoInfo.abaInicial.rota.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase()
            + '/' + dados.cotacaoInfo.sqCdCotac
          ]);
          return false;
        }), catchError(() => of(false)));
    }

    if (abaDestino.id <= cotacaoInfo.abaInicial.id) {
      if (cotacaoInfo.cdSitucCotac === 'TRA' && abaDestino.id !== cotacaoInfo.abaInicial.id) {
        mensagemNavegacao.danger = ['Não é possível navegar nessa proposta, pois ela foi contratada.'
                                 + ' Caso queira consultar os dados, use o documento de proposta.'];
        this.mensagensService.mensagensEmitter.emit(mensagemNavegacao);
        return false;
      }

      if (this.isRetornoStatus(abaDestino)) {
        if (confirm('A proposta será retornada para aba "' + abaDestino.descricao + '".\nIsso pode implicar em: '
                  + '\n\n • Reanálise dos Pretendentes \n • Mudança de valores no cálculo \n\nDeseja prosseguir?')) {
          return this.cotacaoService
            .retornar(cotacaoInfo.sqCdCotac, abaDestino.rota)
            .pipe(map(() => true), catchError(() => of(false)));
        } else {
          return false;
        }
      }
    }
    return true;
  }

  private isRetornoStatus(destino: Aba) {
    const cotacaoInfo = this.cotacaoInfoService.cotacaoInfo;
    if (cotacaoInfo.cdSitucCotac === 'EFE' && destino.id < cotacaoInfo.abaInicial.id) {
      return true;
    }
    if (cotacaoInfo.cdSitucCotac === 'CAL' && destino.id < cotacaoInfo.abaInicial.id) {
      return true;
    }
    if ((destino.rota === 'dadosItem' || destino.rota === 'fichaCadastral') && cotacaoInfo.abaInicial.id > 2) {
      return true;
    }

    return false;
  }

}
