import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad,
         Route, UrlSegment, ActivatedRouteSnapshot,
         RouterStateSnapshot, Router } from '@angular/router';
import { LoginService } from '../services/login/login.service';


@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(private loginService: LoginService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.loginService.isLogado()) {
      if (state.url === '/login' || state.url === '/') {
        this.router.navigate(['/home']);
      }
      return true;
    } else if (state.url === '/login' || state.url === '/') {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(next, state);
  }

  canLoad(route: Route, segments: UrlSegment[]): boolean {
    if (this.loginService.isLogado()) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}
