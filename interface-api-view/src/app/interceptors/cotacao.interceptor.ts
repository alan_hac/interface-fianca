import { DadosCotacaoAPI } from 'src/app/models/api/response/dados-cotacao-api';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { CotacaoInfoService } from 'src/app/services/cotador/comum/cotacao-info.service';
import { tap, catchError } from 'rxjs/operators';
import { MensagensService } from '../services/cotador/comum/mensagens.service';
import { ResponseAPI } from '../models/api/response/response-api';

@Injectable()
export class CotacaoInterceptor implements HttpInterceptor {

  constructor(private cotacaoInfoService: CotacaoInfoService, private mensagensService: MensagensService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const header: { [name: string]: string } = {};

    if (this.cotacaoInfoService.cotacaoInfo) {
      (header.sqCdCotac = this.cotacaoInfoService.cotacaoInfo.sqCdCotac.toString()),
      (header.token = this.cotacaoInfoService.cotacaoInfo.token);
    }

    req = req.clone({ setHeaders: header });

    return next.handle(req).pipe(
      tap(
        httpEvent => {
          if (httpEvent instanceof HttpResponse) {
            if (httpEvent.body && httpEvent.body.abas) {
              this.cotacaoInfoService.abasNavegacao = (httpEvent.body as DadosCotacaoAPI).abas;
            }
            this.cotacaoInfoService.cotacaoInfoFromResponse = httpEvent.body;
            this.mensagensService.trataMensagensRetorno(httpEvent);
          }
        }
      ),
      catchError((res: HttpEvent<any>) => {
        this.mensagensService.trataMensagensRetorno(res);
        return throwError(res);
      })
    );
  }

}
