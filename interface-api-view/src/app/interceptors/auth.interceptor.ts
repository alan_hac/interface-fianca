import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

import { LoginService } from 'src/app/services/login/login.service';
import { CotacaoInfoService } from 'src/app/services/cotador/comum/cotacao-info.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private loginService: LoginService, private cotacaoInfoService: CotacaoInfoService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const header: { [name: string]: string } = {};

    if (this.loginService.isLogado) {
      header.codigoCorretor = this.loginService.login.corretor;
      header.codigoParceiro = this.loginService.login.parceiro ? this.loginService.login.parceiro : '';
      header.senhaOperadora = this.loginService.login.senha;
      header.cpfUsuario = this.loginService.login.cpfUsuario ? this.loginService.login.cpfUsuario.toString() : '' ;
    }

    if (this.cotacaoInfoService.cotacaoInfo) {
      (header.sqCdCotac = this.cotacaoInfoService.cotacaoInfo.sqCdCotac.toString()),
      (header.token = this.cotacaoInfoService.cotacaoInfo.token);
    }

    req = req.clone({ setHeaders: header });

    return next.handle(req);
  }

}
