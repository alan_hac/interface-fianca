import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { PropostaInfo } from 'src/app/models/api/response/proposta/proposta-info';
import { take } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PropostaService {
  private url: string = environment.url + 'proposta/';

  constructor(private http: HttpClient) { }

  getResumoProposta() {
    return this.http.get<PropostaInfo>(this.url).pipe(take(1));
  }

  contratar() {
    return this.http.get<PropostaInfo>(this.url + 'contratar').pipe(take(1));
  }
}
