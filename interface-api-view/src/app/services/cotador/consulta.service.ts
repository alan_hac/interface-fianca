import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Consulta } from 'src/app/models/api/response/consulta';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  private url = environment.url + 'consulta/';

  constructor(private http: HttpClient) { }

  getOpcoes(codQuestionario: number, codQuestao: number, resposta: string) {
    return this.http.get<Consulta>(this.url + 'obterOpcoes/' + codQuestionario + '/' + codQuestao + '/' + resposta)
      .pipe(take(1));
  }
}
