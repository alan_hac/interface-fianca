import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { ListaPretendentes } from 'src/app/models/api/response/ficha-cadastral/lista-pretendentes';
import { FormularioAPreencher } from 'src/app/models/api/response/comum/formulario-a-responder';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { ListaDocumentos } from 'src/app/models/api/response/ficha-cadastral/lista-documentos';

@Injectable({
  providedIn: 'root'
})
export class FichaCadastralService {

  private url = environment.url + 'fichaCadastral/';

  constructor(private http: HttpClient) { }

  getResumoPretendentes() {
    return this.http.get<ListaPretendentes>(this.url)
      .pipe(take(1));
  }

  getDadosPretendente(idPretendente) {
    return this.http.get<FormularioAPreencher>(this.url + idPretendente)
      .pipe(take(1));
  }

  putPretendente(idPretendente, respostas) {
    return this.http.put<ListaPretendentes>(this.url + idPretendente, respostas)
      .pipe(take(1));
  }

  postPretendente(idPretendente, respostas) {
    return this.http.post<ListaPretendentes>(this.url + idPretendente, respostas)
      .pipe(take(1));
  }

  deletePretendente(idPretendente) {
    return this.http.delete<ListaPretendentes>(this.url + idPretendente)
      .pipe(take(1));
  }

  validarPretendentes() {
    return this.http.get<ListaPretendentes>(this.url + 'validar')
      .pipe(take(1));
  }

  getDadosDocumentosPretendente(idPretendente) {
    return this.http.get<ListaDocumentos>(this.url + 'listarDocumentos/' + idPretendente)
      .pipe(take(1));
  }

  solicitarCadastroPretendente() {
    return this.http.get<FormularioAPreencher>(this.url + 'preenchimentoPretendente/solicitar')
      .pipe(take(1));
  }

  reenviarCadastroPretendente() {
    return this.http.get<FormularioAPreencher>(this.url + 'preenchimentoPretendente/reenviar')
      .pipe(take(1));
  }

  cancelarCadastroPretendente() {
    return this.http.get<FormularioAPreencher>(this.url + 'preenchimentoPretendente/cancelar')
      .pipe(take(1));
  }

  uploadDocumento(idDocto: number, documento: File) {
    const formData = new FormData();
    formData.append('arquivo', documento, documento.name);
    return this.http
      .post<FormularioAPreencher>(this.url + 'uploadDocumento/' + idDocto, formData, { reportProgress: true, observe: 'events' })
      .pipe(take(1));
  }

  downloadDocumento(idDocto: number, nomeArquivo: string) {
    return this.http
      .get(this.url + 'downloadDocumento/' + idDocto, {responseType: 'blob', observe: 'response'})
      .pipe(take(1))
      .subscribe(resp => this.abrirDocumento(resp, nomeArquivo));
  }

  abrirDocumento(resp, nomeArquivo): void {
    const fileURL = URL.createObjectURL(resp.body);
    const element = document.createElement('a');
    element.href = fileURL;
    element.download = resp.headers.get('File-Name') ? resp.headers.get('File-Name') : nomeArquivo;
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
    URL.revokeObjectURL(fileURL);
  }
}
