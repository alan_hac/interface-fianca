import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { take } from 'rxjs/operators';
import { FormularioAPreencher } from 'src/app/models/api/response/comum/formulario-a-responder';
import { ListaComplemento } from 'src/app/models/api/response/complemento/lista-complemento';

@Injectable({
  providedIn: 'root'
})
export class ComplementoService {
  private url: string = environment.url + 'complemento/';

  constructor(private http: HttpClient) { }

  listDadosComplemento() {
    return this.http.get<ListaComplemento>(this.url)
      .pipe(take(1));
  }

  getDadosLocacao() {
    return this.http.get<FormularioAPreencher>(this.url + 'dadosLocacao')
      .pipe(take(1));
  }

  postDadosLocacao(respostas) {
    return this.http.post<ListaComplemento>(this.url + 'dadosLocacao', respostas)
      .pipe(take(1));
  }

  getDadosSegurado() {
    return this.http.get<FormularioAPreencher>(this.url + 'segurado')
      .pipe(take(1));
  }

  postDadosSegurado(respostas) {
    return this.http.post<ListaComplemento>(this.url + 'segurado', respostas)
      .pipe(take(1));
  }

  getDadosPretendente(idGarantido) {
    return this.http.get<FormularioAPreencher>(this.url + 'garantido/' + idGarantido)
      .pipe(take(1));
  }

  postDadosPretendente(idGarantido, respostas) {
    return this.http.post<ListaComplemento>(this.url + 'garantido/' + idGarantido, respostas)
      .pipe(take(1));
  }

  validateComplemento() {
    return this.http.get<FormularioAPreencher>(this.url + 'validar')
      .pipe(take(1));
  }
}
