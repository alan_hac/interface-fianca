import { CotacaoInfoService } from 'src/app/services/cotador/comum/cotacao-info.service';

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageableSpring } from 'src/app/models/api/response/comum/pageable-spring';
import { DadosCotacaoAPI } from 'src/app/models/api/response/dados-cotacao-api';
import { take } from 'rxjs/operators';
import { PageRequest } from 'src/app/models/api/request/page';

@Injectable({
  providedIn: 'root'
})

// https://servicos-aceitew.tokiomarine.com.br/massificados/CotadorFiancaService/api/swagger-ui.html#/01_-_Cotacao_Controller
export class CotacaoService {

  private url = environment.url + 'cotacao/';

  constructor(private http: HttpClient) { }

  obterNovaCotacao(tpPessoa: string, icResidencial: string) {
    return this.http.get<DadosCotacaoAPI>(this.url + 'iniciar/' + tpPessoa + '/' + icResidencial + '?anti-cache=' + new Date().getTime())
      .pipe(take(1));
  }

  obterCotacaoExistente(cotacao: number) {
    return this.http.get<DadosCotacaoAPI>(this.url + 'editar/' + cotacao + '?anti-cache=' + new Date().getTime())
      .pipe(take(1));
  }

  listaCotacoes(pageable: PageRequest): Observable<PageableSpring> {
    return this.http.get<PageableSpring>(this.url + 'listar', { params: this.trataPageable(pageable) })
      .pipe(take(1));
  }

  retornar(cotacao: number, abaDestino: string) {
    return this.http.get<DadosCotacaoAPI>(this.url + 'retornar/' + cotacao + '/' + abaDestino)
      .pipe(take(1));
  }

  trataPageable(pageable: PageRequest): HttpParams {
    let httpParams: HttpParams = new HttpParams();
    if (!pageable) {
      return httpParams;
    }
    if (pageable.start) {
      pageable.start = pageable.start.replace(/\//gm, '');
    }
    if (pageable.end) {
      pageable.end = pageable.end.replace(/\//gm, '');
    }

    Object.keys(pageable).forEach(param => {
      if (pageable[param]) {
          httpParams = httpParams.set(param, pageable[param]);
      }
    });
    return httpParams;
  }

}
