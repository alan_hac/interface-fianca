import { ResponseAPI } from '../../../models/api/response/response-api';
import { Injectable, EventEmitter } from '@angular/core';
import { CotacaoInfo } from 'src/app/models/api/response/comum/cotacao-info';
import { Aba } from 'src/app/models/api/response/comum/aba';

@Injectable({
  providedIn: 'root'
})
export class CotacaoInfoService {

  public cotacaoEmitter: EventEmitter<CotacaoInfo> = new EventEmitter<CotacaoInfo>();
  private COTACAO_INFO: CotacaoInfo = null;
  private ABAS_NAVEGACAO: Aba[] = new Array<Aba>();

  constructor() { }

  get cotacaoInfo(): CotacaoInfo {
    return this.COTACAO_INFO;
  }

  set cotacaoInfo(cotacao: CotacaoInfo) {
    this.COTACAO_INFO = cotacao;
    this.cotacaoEmitter.emit(this.COTACAO_INFO);
  }

  get abasNavegacao(): Aba[] {
    return this.ABAS_NAVEGACAO;
  }

  set abasNavegacao(abas: Aba[]) {
    this.ABAS_NAVEGACAO = abas;
  }

  set cotacaoInfoFromResponse(res: ResponseAPI) {
    if (res && res.cotacaoInfo) {
      this.cotacaoInfo = res.cotacaoInfo;
    }
  }

  get rotaAtual() {
    return '/cotador/'
            + this.COTACAO_INFO.abaInicial.rota.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase() + '/'
            + this.COTACAO_INFO.sqCdCotac;
  }

  get situacao() {
    if (!this.COTACAO_INFO || !this.COTACAO_INFO.cdSitucCotac) {
      return null;
    }
    switch (this.COTACAO_INFO.cdSitucCotac) {
      case 'PEN':
        return 'Pendente';
      case 'ANA':
        return 'Em Análise';
      case 'CAL':
        return 'Calculado';
      case 'EFE':
        return 'Efetivado';
      case 'TRA':
        return 'Contratada';
      case 'REN':
        return 'Renovação';
      case 'REC':
        return 'Recusada';
      case 'CAD':
        return 'Cad. Pret.';
    }
  }
}
