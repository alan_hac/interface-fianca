import { HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { LoginService } from 'src/app/services/login/login.service';
import { Mensagens } from 'src/app/models/api/response/mensagens';

@Injectable({
  providedIn: 'root'
})
export class MensagensService {

  public mensagensEmitter: EventEmitter<Mensagens> = new EventEmitter<Mensagens>();

  constructor(private loginService: LoginService) { }

  trataMensagensRetorno(res: HttpEvent<any>): void {
    let mensagens = new Mensagens();
    if (res instanceof HttpErrorResponse) {
      const resError = res as HttpErrorResponse;
      if (resError.status === 412 && resError.error) {
        mensagens = resError.error.mensagens;
      } else {
        let erroGenerico: string;
        if (resError.status === 403) {
          this.loginService.invalidaLogin();
        } else if (resError.status === 404 || resError.status === 405 || resError.status === 0) {
          erroGenerico = 'Não foi possível acessar o servidor. Não há conexão de rede ou o servidor de destino está fora do ar.';
        } else {
          erroGenerico = 'Falha interna do servidor. Favor entrar em contato com o a área de atendimento da Tokio Marine Seguradora';
        }
        mensagens = new Mensagens([erroGenerico]);
      }
    }
    if (res instanceof HttpResponse) {
      const resSuccess = res as HttpResponse<any>;
      if (resSuccess.body && resSuccess.body.mensagens) {
        mensagens = resSuccess.body.mensagens;
      } else {
        this.mensagensEmitter.emit(new Mensagens());
      }
    }

    if (this.contemMensagem(mensagens)) {
      this.mensagensEmitter.emit(mensagens);
    }

  }

  private contemMensagem(mensagens): boolean {
    return mensagens && (mensagens.danger && mensagens.danger.length > 0)
        || (mensagens.info && mensagens.info.length > 0)
        || (mensagens.warning && mensagens.warning.length > 0)
        || (mensagens.success && mensagens.success.length > 0);
  }

}
