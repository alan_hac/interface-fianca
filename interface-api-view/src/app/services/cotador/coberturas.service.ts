import { RespostasCoberturas } from './../../models/api/request/respostas-coberturas';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { RespostasFormulario } from '../../models/api/request/respostas-formulario';
import { FormularioAPreencher } from '../../models/api/response/comum/formulario-a-responder';
import { ListaCoberturas } from 'src/app/models/api/response/coberturas/lista-coberturas';

@Injectable({
  providedIn: 'root'
})
export class CoberturasService {

  private url = environment.url + 'coberturas/';

  constructor(private http: HttpClient) { }

  getDadosLocacao() {
    return this.http.get<FormularioAPreencher>(this.url + 'dadosLocacao')
      .pipe(take(1));
  }

  postDadosLocacao(respostas: RespostasFormulario) {
    return this.http.post<ListaCoberturas>(this.url + 'dadosLocacao', respostas)
      .pipe(take(1));
  }

  getDadosCoberturas() {
    return this.http.get<ListaCoberturas>(this.url)
      .pipe(take(1));
  }

  postDadosCoberturas(respostas: RespostasCoberturas) {
    return this.http.post<FormularioAPreencher>(this.url, respostas)
      .pipe(take(1));
  }
}
