import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { RespostasFormulario } from '../../models/api/request/respostas-formulario';
import { FormularioAPreencher } from '../../models/api/response/comum/formulario-a-responder';

@Injectable({
  providedIn: 'root'
})
export class DadosBasicosService {

  private url = environment.url + 'dadosItem/';

  constructor(private http: HttpClient) { }

  getDadosBasicos() {
    return this.http.get<FormularioAPreencher>(this.url)
      .pipe(take(1));
  }

  postDadosBasicos(respostas: RespostasFormulario) {
    return this.http.post<FormularioAPreencher>(this.url, respostas)
      .pipe(take(1));
  }

}
