import { CotacaoInfoService } from './comum/cotacao-info.service';
import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ImpressaoService {

  private url = environment.url + 'impressao/';

  constructor(private http: HttpClient, private cotacaoInfoService: CotacaoInfoService) { }

  getArquivoCotacao() {
    return this.http
      .get(this.url + 'cotacao', { responseType: 'blob', observe: 'response'})
      .pipe(take(1))
      .subscribe(impressao => this.abrirPDF(impressao.body, 'Cotação - ' + this.cotacaoInfoService.cotacaoInfo.sqCdCotac));
  }

  getArquivoFichaCadastral() {
    return this.http
    .get(this.url + 'fichaCadastral', { responseType: 'blob', observe: 'response'})
    .pipe(take(1))
    .subscribe(impressao => this.abrirPDF(impressao.body, 'Ficha Cadastral - ' + this.cotacaoInfoService.cotacaoInfo.sqCdCotac));
  }

  getArquivoCartaParcer() {
    return this.http
    .get(this.url + 'cartaParecer', { responseType: 'blob', observe: 'response'})
    .pipe(take(1))
    .subscribe(impressao => this.abrirPDF(impressao.body, 'Carta Parecer - ' + this.cotacaoInfoService.cotacaoInfo.sqCdCotac));
  }

  getArquivoProposta() {
    return this.http
    .get(this.url + 'proposta', { responseType: 'blob', observe: 'response'})
    .pipe(take(1))
    .subscribe(impressao => this.abrirPDF(impressao.body, 'Proposta - ' + this.cotacaoInfoService.cotacaoInfo.sqCdCotac));
  }

  getArquivoApoliceCompleta() {
    return this.http
    .get(this.url + 'pos-contratacao/apoliceCompleta', { responseType: 'blob', observe: 'response'})
    .pipe(take(1))
    .subscribe(impressao => this.abrirPDF(impressao.body, 'Apólice Completa - ' + this.cotacaoInfoService.cotacaoInfo.sqCdCotac));
  }

  getArquivoApoliceResumida() {
    return this.http
    .get(this.url + 'pos-contratacao/apoliceResumida', { responseType: 'blob', observe: 'response'})
    .pipe(take(1))
    .subscribe(impressao => this.abrirPDF(impressao.body, 'Apolice Resumida - ' + this.cotacaoInfoService.cotacaoInfo.sqCdCotac));
  }

  getArquivoCartaoSegurado() {
    return this.http
    .get(this.url + 'pos-contratacao/cartaoSegurado', { responseType: 'blob', observe: 'response'})
    .pipe(take(1))
    .subscribe(impressao => this.abrirPDF(impressao.body, 'Cartão Segurado - ' + this.cotacaoInfoService.cotacaoInfo.sqCdCotac));
  }

  getArquivoCartaoGarantido() {
    return this.http
    .get(this.url + 'pos-contratacao/cartaoGarantido', { responseType: 'blob', observe: 'response'})
    .pipe(take(1))
    .subscribe(impressao => this.abrirPDF(impressao.body, 'Cartão Garantido - ' + this.cotacaoInfoService.cotacaoInfo.sqCdCotac));
  }

  abrirPDF(pdf, documento) {
    const fileURL = URL.createObjectURL(pdf);
    if (window.navigator && window.navigator.msSaveOrOpenBlob) { // Para funcionar no maldito IE
      window.navigator.msSaveOrOpenBlob(pdf, documento);
    } else {
      window.open(fileURL);
    }

  }
}
