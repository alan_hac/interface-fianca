import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { CalculosInfo } from 'src/app/models/api/response/calculo/calculos-info';
import { SelecaoCalculo } from 'src/app/models/api/request/selecao-calculo';
import { RespostasRecalculo } from 'src/app/models/api/request/respostas-recalculo';


@Injectable({
  providedIn: 'root'
})
export class CalculoService {

  private url = environment.url + 'calculo/';

  constructor(private http: HttpClient) { }

  getCalculo() {
    return this.http.get<CalculosInfo>(this.url)
      .pipe(take(1));
  }

  postCalculo(respostas: SelecaoCalculo) {
    return this.http.post<string>(this.url, respostas)
      .pipe(take(1));
  }

  recalcular(comissao: RespostasRecalculo) {
    return this.http.post<CalculosInfo>(this.url + 'recalcular', comissao)
      .pipe(take(1));
  }
}
