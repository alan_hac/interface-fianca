import { Injectable } from '@angular/core';
import { Login } from 'src/app/models/login/login';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private _LOGIN: Login = new Login();

  constructor(private router: Router) {
    const loginStorage = localStorage.getItem('login');
    if (loginStorage) {
      this._LOGIN = JSON.parse(loginStorage);
    }
  }

  isLogado(): boolean {
    return (!!this._LOGIN && !!this._LOGIN.corretor && !!this._LOGIN.cpfUsuario && !!this._LOGIN.senha);
  }

  invalidaLogin(): void {
    this._LOGIN.senha = null;
    this.login = this._LOGIN;
    this.router.navigate(['/login']);
  }

  get login(): Login {
    return this._LOGIN;
  }

  set login(login: Login) {
    localStorage.setItem('login', JSON.stringify(login));
    this._LOGIN = login;
  }
}
