import { DadosCotacaoAPI } from './../models/api/response/dados-cotacao-api';
import { CotacaoService } from './../services/cotador/cotacao.service';
import { CotacaoInfoService } from './../services/cotador/comum/cotacao-info.service';
import { CotacaoInfo } from 'src/app/models/api/response/comum/cotacao-info';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CotacaoInfoResolver implements Resolve<CotacaoInfo> {

  constructor(private cotacaoInfoService: CotacaoInfoService, private cotacaoService: CotacaoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): CotacaoInfo | Observable<CotacaoInfo> | Promise<CotacaoInfo> {
    const endpoint = state.url.split('/').slice(-1)[0];
    if (endpoint === 'home') {
      return of(null);
    }
    const sqCdCotac = Number( endpoint );
    if (this.cotacaoInfoService.cotacaoInfo ===  null || this.cotacaoInfoService.cotacaoInfo.sqCdCotac !== sqCdCotac) {
      return this.cotacaoService.obterCotacaoExistente(sqCdCotac)
                                .pipe(map((res: DadosCotacaoAPI) => res.cotacaoInfo));
    }

    return this.cotacaoInfoService.cotacaoInfo;
  }

}
