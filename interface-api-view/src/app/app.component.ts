import { Component, AfterViewInit } from '@angular/core';
import { LoginService } from './services/login/login.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import * as mat from 'materialize-css';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { LoadingComponent } from './components/loading/loading.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  @BlockUI() blockUI: NgBlockUI;
  blockTemplate = LoadingComponent;

  constructor(private loginService: LoginService, private router: Router) {}

  ngAfterViewInit() {
    mat.Sidenav.init($('.sidenav'));
  }

  get corretor(): string {
    return 'Corretor: ' + this.loginService.login.corretor
      + (this.loginService.login.parceiro ? ' - Parceiro: ' + this.loginService.login.parceiro : '')
      + ' - CPF: ' + this.loginService.login.cpfUsuario;
  }

  get isLogado(): boolean {
    return this.loginService.isLogado();
  }

  public logOut() {
    this.loginService.invalidaLogin();
    this.router.navigate(['/login']);
  }

}
