import { AbstractControl, ValidationErrors } from '@angular/forms';

export class RDValidators {
  static moeda(control: AbstractControl): ValidationErrors | null {
    if (isNaN(control.value)) {
      control.value.toString().trim().split('.').join('').split(',').join('.');
    }
    return null;
  }
}
