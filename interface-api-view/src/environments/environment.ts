export const environment = {
  production: false,
  // Aceite W
  url: 'https://servicos-aceitew.tokiomarine.com.br/massificados/CotadorFiancaService/api/'

  // Localhost
  // url: 'http://localhost:8080/massificados/CotadorFiancaService/api/'
};
