package br.com.tokiomarine.fianca.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
	@GetMapping(value = "/{[path:[^\\.]*}")
	public String redirect() throws IOException {
		return "forward:/";
	}
}
